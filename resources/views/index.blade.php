<!DOCTYPE html>
  <html>
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Sinjufego - Cadastro</title>

    <link rel="shortcut icon" type="image/png" href="{{asset('assets/img/favicon.ico')}}"/>
    
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="{{asset('assets/css/style.css')}}"  media="screen,projection"/>

    </head>

    <body>
      <div class="container">
        <div class="section">

          <div class="row">
            <div class="col-md-12" style="text-align: center;">
              <img src="{{asset('assets/img/logo.png')}}" style="width: 100%;text-align: center;margin-left: auto;margin-right: auto;}">
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-md-offset-3" style="text-align:center;">
              <img src="{{asset('assets/img/cadastro_online.png')}}">
            </div>
          </div>

          @if($errors->any())
          <div class="row">
            <div class="col-md-6 col-md-offset-3">
              <div class="form-alert" role="alert" style="margin-top: 20px;">
                <p>
                  @foreach ($errors->all() as $error)
                    @if($error != "The ddd confirmation does not match." && $error != "The email confirmation does not match." && $error != "The numero confirmation does not match.")
                      <div><i class="material-icons">error_outline</i>{{$error}}</div>
                    @endif
                  @endforeach
                </p>
              </div>
            </div>
          </div>
          @endif

          <div class="row">
            <form class="col-md-6 col-md-offset-3 form-horizontal" method="POST" action="salvarForm" style="margin-top: 20px;">
              <input type="hidden" name="_token" value="{{ csrf_token() }}">

              <div class="row">
                <div class="form-group col-md-12">
                  <label for="nome" class="control-label col-md-3">Nome</label>
                  <div class="col-md-9">
                    <input placeholder=" " name="nome" id="nome" type="text" class="form-control" value="{{ old('nome') }}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label class="control-label col-md-3">Sexo</label>
                  <div class="col-md-9">
                    <select id="sexo" name="sexo" class="form-control">
                      <option value="" disabled selected>Selecione o sexo</option>
                      <option value="masculino" {{old('sexo') == 'masculino' ? "selected" : ""}}>Masculino</option>
                      <option value="feminino" {{old('sexo') == 'feminino' ? "selected" : ""}}>Feminino</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label for="email" class="control-label col-md-3">Email</label>
                  <div class="col-md-9">
                    <input placeholder=" " id="email" type="email" class="validate form-control" name="email" value="{{ old('email') }}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label for="email_confirmation" class="control-label col-md-3">Confirme o Email</label>
                  <div class="col-md-9">
                    <input placeholder=" " id="email_confirmation" type="email" class="form-control" name="email_confirmation" value="{{ old('email_confirmation') }}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <input type="hidden" name="ddd_selecionado" id="ddd_selecionado" value="{{old('ddd')}}">
                  <label class="control-label col-md-3">Celular</label>
                  <div class="col-md-3">
                    <select id="ddd" name="ddd" class="form-control">
                      <option value="" disabled selected>DDD</option>
                      <option value="62">62</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="24">24</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="31">31</option>
                      <option value="32">32</option>
                      <option value="33">33</option>
                      <option value="34">34</option>
                      <option value="35">35</option>
                      <option value="37">37</option>
                      <option value="38">38</option>
                      <option value="41">41</option>
                      <option value="42">42</option>
                      <option value="43">43</option>
                      <option value="44">44</option>
                      <option value="45">45</option>
                      <option value="46">46</option>
                      <option value="47">47</option>
                      <option value="48">48</option>
                      <option value="49">49</option>
                      <option value="51">51</option>
                      <option value="53">53</option>
                      <option value="54">54</option>
                      <option value="55">55</option>
                      <option value="61">61</option>
                      <option value="63">63</option>
                      <option value="64">64</option>
                      <option value="65">65</option>
                      <option value="66">66</option>
                      <option value="67">67</option>
                      <option value="68">68</option>
                      <option value="69">69</option>
                      <option value="71">71</option>
                      <option value="73">73</option>
                      <option value="74">74</option>
                      <option value="75">75</option>
                      <option value="77">77</option>
                      <option value="79">79</option>
                      <option value="81">81</option>
                      <option value="82">82</option>
                      <option value="83">83</option>
                      <option value="84">84</option>
                      <option value="85">85</option>
                      <option value="86">86</option>
                      <option value="87">87</option>
                      <option value="88">88</option>
                      <option value="89">89</option>
                      <option value="91">91</option>
                      <option value="92">92</option>
                      <option value="93">93</option>
                      <option value="94">94</option>
                      <option value="95">95</option>
                      <option value="96">96</option>
                      <option value="97">97</option>
                      <option value="98">98</option>
                      <option value="99">99</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                    <input placeholder="Número" name="numero" id="numero" type="text" class="form-control" maxlength="9" size="9" value="{{ old('numero') }}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                <input type="hidden" name="ddd_confirmation_selecionado" id="ddd_confirmation_selecionado" value="{{old('ddd_confirmation')}}">
                  <label class="control-label col-md-3">Confirme o celular</label>
                  <div class="col-md-3">
                    <select id="ddd_confirmation" name="ddd_confirmation" class="form-control">
                      <option value="" disabled selected>DDD</option>
                      <option value="62">62</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="24">24</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="31">31</option>
                      <option value="32">32</option>
                      <option value="33">33</option>
                      <option value="34">34</option>
                      <option value="35">35</option>
                      <option value="37">37</option>
                      <option value="38">38</option>
                      <option value="41">41</option>
                      <option value="42">42</option>
                      <option value="43">43</option>
                      <option value="44">44</option>
                      <option value="45">45</option>
                      <option value="46">46</option>
                      <option value="47">47</option>
                      <option value="48">48</option>
                      <option value="49">49</option>
                      <option value="51">51</option>
                      <option value="53">53</option>
                      <option value="54">54</option>
                      <option value="55">55</option>
                      <option value="61">61</option>
                      <option value="63">63</option>
                      <option value="64">64</option>
                      <option value="65">65</option>
                      <option value="66">66</option>
                      <option value="67">67</option>
                      <option value="68">68</option>
                      <option value="69">69</option>
                      <option value="71">71</option>
                      <option value="73">73</option>
                      <option value="74">74</option>
                      <option value="75">75</option>
                      <option value="77">77</option>
                      <option value="79">79</option>
                      <option value="81">81</option>
                      <option value="82">82</option>
                      <option value="83">83</option>
                      <option value="84">84</option>
                      <option value="85">85</option>
                      <option value="86">86</option>
                      <option value="87">87</option>
                      <option value="88">88</option>
                      <option value="89">89</option>
                      <option value="91">91</option>
                      <option value="92">92</option>
                      <option value="93">93</option>
                      <option value="94">94</option>
                      <option value="95">95</option>
                      <option value="96">96</option>
                      <option value="97">97</option>
                      <option value="98">98</option>
                      <option value="99">99</option>
                    </select>
                  </div>
                  <div class="col-md-6">
                  <input placeholder="Número" name="numero_confirmation" id="numero_confirmation" type="text" class="form-control" maxlength="9" size="9" value="{{ old('numero_confirmation') }}">
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label class="control-label col-md-3">Aposentado?</label>
                  <div class="col-md-9">
                    <select id="aposentado" name="aposentado" class="form-control">
                      <option value="">Selecione</option>
                      <option value="sim" {{old('aposentado') == 'sim' ? "selected" : ""}}>Sim</option>
                      <option value="nao" {{old('aposentado') == 'nao' ? "selected" : ""}}>Não</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <label class="control-label col-md-3">Tribunal onde está lotado</label>
                  <div class="col-md-9">
                    <select id="tribunal" name="tribunal" class="form-control">
                      <option value="" disabled selected>Selecione o tribunal</option>
                      @foreach($tribunais as $tribunal)
                        <option value="{{$tribunal->tribunal}}" {{$tribunal->tribunal == old('tribunal') ? "selected" : ""}}>{{$tribunal->tribunal}}</option>
                      @endforeach
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group col-md-12">
                  <input type="hidden" name="cidade_selecionada" id="cidade_selecionada" value="{{old('cidade')}}">
                  <label class="control-label col-md-3">Cidade onde está lotado</label>
                  <div class="col-md-9">
                    <select id="cidade" name="cidade" data-cidade="{{old('cidade')}}" class="form-control">
                      <option class="placehold" value="" disabled selected>Selecione a cidade</option>
                    </select>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="form-group" style="">
                    <div class="col-md-3 col-md-offset-9">
                  <button type="submit" class="btn btn-primary" style="">Concluir</button>
                  </div>
                </div>
              </div>

            </form>
          </div>


        </div>
      </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
    </body>
  </html>