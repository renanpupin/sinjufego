@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    @if($tipo == 'cadastro')
                        <h3>Controle de Filiações Online</h3>
                    @else
                        <h3>Recadastramento de Filiados</h3>
                    @endif
                </div>

                <div class="panel-body">
                    <div class="table-resonsive">
                        <table class="table table-bordered table-stripped">
                            <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Data Hora</th>
                                    <th>Nome</th>
                                    <th>Sexo</th>
                                    <th>Email</th>
                                    <th>DDD</th>
                                    <th>Celular</th>
                                    <th>Tribunal</th>
                                    <th>Aposentado</th>
                                    <th>Cidade</th>
                                    <th>OK</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($membros as $membro)
                                    <tr>
                                        <td>{{$membro->id}}</td>
                                        <td>{{$membro->data_hora}}</td>
                                        <td>{{$membro->nome}}</td>
                                        <td>{{$membro->sexo}}</td>
                                        <td>{{$membro->email}}</td>
                                        <td>{{$membro->ddd}}</td>
                                        <td>{{$membro->celular}}</td>
                                        <td>{{$membro->tribunal}}</td>
                                        <td>{{$membro->aposentado}}</td>
                                        <td>{{$membro->cidade}}</td>
                                        <td>{{$membro->ok}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12" style="text-align: center;">
            {!! $membros->render() !!}
        </div>
    </div>
</div>


@endsection
