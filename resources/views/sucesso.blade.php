<!DOCTYPE html>
  <html>
    <head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <title>Sinjufego - Cadastro</title>

    <link rel="shortcut icon" type="image/png" href="{{asset('assets/img/favicon.ico')}}"/>
    
      <!--Import Google Icon Font-->
      <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
      <!--Import materialize.css-->
      <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
      <link type="text/css" rel="stylesheet" href="{{asset('assets/css/style.css')}}"  media="screen,projection"/>

    </head>

    <body>
      <div class="container">
        <div class="section">

          <div class="row">
            <div class="col-md-12" style="text-align: center;">
              <img src="{{asset('assets/img/logo.png')}}" style="width: 100%;text-align: center;margin-left: auto;margin-right: auto;}">
            </div>
          </div>

          <div class="row">
            <div class="col-md-6 col-md-offset-3" style="text-align:center;">
              <img src="{{asset('assets/img/cadastro_online.png')}}">
            </div>
          </div>

          @if($errors->any())
          <div class="row">
            <div class=" col-md-6 col-md-offset-3">
              <div class="form-alert" role="alert">
                <p><i class="material-icons">error_outline</i>Preecha todos os campos!</p>
              </div>
            </div>
          </div>
          @endif

          <div class="row">
            <div class="col-md-6 col-md-offset-3" style="margin-top: 30px; text-align: center;">
                <p>Olá <b>{{$nome}}</b>, sua filiação online foi efetuada com sucesso!</p>
                <p>Seja bem vindo ao Sinjufego.</p>
                <p>Aguarde um email de confirmação em sua caixa postal. Obrigado!</p>
            </div>
          </div>


        </div>
      </div>

      <!--Import jQuery before materialize.js-->
      <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
      <script type="text/javascript" src="{{asset('assets/js/script.js')}}"></script>
    </body>
  </html>