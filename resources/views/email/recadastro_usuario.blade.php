<p><b>Recadastramento Efetuado com sucesso !!</b></p>

<p>Caro(a) {{$nome}}</p>

<p>Obrigado pela atualização do seus dados cadastrais.</p>

<p>Manter seus dados cadastrais atualizados é fundamental para receber em dia as notícias do seu Sindicato.</p>

<p>Muito Obrigado.</p>

<p>Leopoldo Donizete de Lima</p>

<p>Presidente</p>

<p>-------------------- FICHA DE FILIAÇÃO ----------------------</p>
<p></p>

<b>
<p></p>
<p>Data: {{$data}}</p>
<p></p>
<p>Nome: {{$nome}}</p>
<p>Sexo: {{$sexo}}</p>
<p>Email: {{$email}}</p>
<p>Celular {{$celular}}</p>
<p>Aposentado : {{$aposentado}}</p>
<p>Tribunal: {{$tribunal}}</p>
<p>Cidade: {{$cidade}}</p>
</b>
<p>----------------------------------------------------------------------------</p>