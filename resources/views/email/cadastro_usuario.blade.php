<p><b>Filiação "On-Line" Efetuada com sucesso !!</b></p>

<p>Caro(a) {{$nome}}</p>

<p>Seja muito bem vindo(a) ao Sinjufego.</p>

<p>Agradecemos a sua participação.</p>

<p>Leopoldo Donizete de Lima</p>

<p>Presidente</p>

<p>-------------------- FICHA DE FILIAÇÃO ----------------------</p>
<p></p>

<b>
<p>Declaro para os devidos fins que, no presente ato, <br>
estou ingressando como filiado ao Sinjufego, e autorizo o <br>
desconto de 0,5% (meio por cento) sobre minha remuneração, <br>
conforme definido em Assembléia Geral.</p>
<p></p>
<p>Data: {{$data}}</p>
<p></p>
<p>Nome: {{$nome}}</p>
<p>Sexo: {{$sexo}}</p>
<p>Email: {{$email}}</p>
<p>Celular {{$celular}}</p>
<p>Aposentado : {{$aposentado}}</p>
<p>Tribunal: {{$tribunal}}</p>
<p>Cidade: {{$cidade}}</p>
</b>
<p>----------------------------------------------------------------------------</p>