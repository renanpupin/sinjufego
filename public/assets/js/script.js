$(document).ready(function() {
    // $('#sexo').material_select();
    // $('#ddd').material_select();
    // $('#ddd_confirmation').material_select();
    // $('#aposentado').material_select();
    // $('#tribunal').material_select();
    // $('#cidade').material_select();

    $('#tribunal').change(function(){
        var nome_tribunal = $(this).val();
        $.ajax({
            url: "buscaCidadesTribunal",
            method: "GET",
            data: {nome_tribunal : nome_tribunal}
        })
        .done(function( cidades ) {
            $("#cidade").find("option").not(".placehold").remove();
            var selected_option = $("#cidade_selecionada").val();
            for(var index = 0; index < cidades.length; index++){
                if(selected_option == cidades[index].cidade){
                    $("#cidade").append('<option value="'+cidades[index].cidade+'" selected>'+cidades[index].cidade+'</option>');
                }else{
                    $("#cidade").append('<option value="'+cidades[index].cidade+'">'+cidades[index].cidade+'</option>');
                }
            }
            // $('#cidade').material_select();
        });
    });

    $('#tribunal').trigger("change");

    function loadSelectedDdd(){
        var ddd_selecionado = $("#ddd_selecionado").val();
        $("#ddd").find("option").each(function(index, elem){
            if(ddd_selecionado == $(elem).val()){
                $(elem).attr("selected","selected");
                // $('#ddd').material_select();
            }
        });
    }
    loadSelectedDdd();

    function loadSelectedDddConfirmation(){
        var ddd_confirmation_selecionado = $("#ddd_confirmation_selecionado").val();
        $("#ddd_confirmation").find("option").each(function(index, elem){
            if(ddd_confirmation_selecionado == $(elem).val()){
                $(elem).attr("selected","selected");
                // $('#ddd_confirmation').material_select();
            }
        });
    }
    loadSelectedDddConfirmation();
  });