<?php
use Illuminate\Database\Seeder;
class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'id' => 2,
                'name' => 'sinjufego',
                'email' => 'administrador@sinjufego.org.br',
                'password' => bcrypt('f3h5D8@9')
            )
        ));
    }
}