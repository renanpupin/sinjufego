<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TribunalCidade extends Model
{
    protected $table = "tribunal_cidade";

    protected $fillable = array(
        'estado',
        'cidade',
        'tribunal'
    );
}