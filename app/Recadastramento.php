<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recadastramento extends Model
{
    protected $table = "cadastro";

    public $timestamps = false;

    protected $connection = 'mysql2';

    protected $fillable = array(
        'data_hora',
        'nome',
        'sexo',
        'email',
        'ddd',
        'celular',
        'tribunal',
        'aposentado',
        'cidade',
        'ok'
    );
}