<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tribunal extends Model
{
    protected $table = "tribunal";

    protected $fillable = array(
        'tribunal'
    );
}