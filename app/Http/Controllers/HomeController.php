<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\Membro;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function cadastros()
    {
        $membros = Membro::orderBy('nome','asc')->paginate(15);

        return view('home')->with('membros',$membros)->with('tipo','cadastro');
    }

    public function recadastros()
    {
        // $membros = Membro::orderBy('nome','asc')->paginate(15);

        $membros = new Membro;

        $membros->setConnection('mysql2');

        $membros = $membros->orderBy('nome','asc')->paginate(15);

        return view('home')->with('membros',$membros)->with('tipo','recadastro');
    }
}
