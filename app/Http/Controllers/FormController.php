<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Controller;
use Illuminate\Pagination\Paginator;
use DB;
use Validator;
use App\Form;
use App\Recadastramento;
use App\Tribunal;
use App\TribunalCidade;
use Carbon;
use Mail;

class FormController extends Controller
{

    public function buscaCidadesTribunal(Request $request){

        $nome_tribunal = $request['nome_tribunal'];

        $cidades = TribunalCidade::where("tribunal",$nome_tribunal)->orderBy('cidade','asc')->get();

        return $cidades;
    }

    public function recadastramento(){

        $tribunais = Tribunal::orderBy('tribunal','asc')->get();

        return view('recadastramento')->with('tribunais',$tribunais);
    }

    public function salvarRecadastramento(Request $request)
    {
        $regras = array(
            'nome' => 'required',
            'sexo' => 'required',
            'email' => 'required|confirmed',
            'email_confirmation' => 'required|same:email',
            'ddd' => 'required|confirmed',
            'numero' => 'required|confirmed',
            'ddd_confirmation' => 'required|same:ddd',
            'numero_confirmation' => 'required|same:numero',
            'aposentado' => 'required',
            'tribunal' => 'required',
            'cidade' => 'required'
        );

        $mensagens = array(
            'required' => 'O campo :attribute é obrigatório.',
            'numero.required' => 'O campo número é obrigatório.',
            'numero_confirmation.required' => 'O campo corfirmação de número é obrigatório.',
            'email_confirmation.required' => 'O campo corfirmação de email é obrigatório.',
            'ddd_confirmation.required' => 'O campo corfirmação de DDD é obrigatório.',
            'email.same' => 'Os campos de email devem coincidir.',
            'email_confirmation.same' => 'Os campos de email devem coincidir.',
            'ddd_confirmation.same' => 'Os campos de DDD devem coincidir.',
            'numero_confirmation.same' => 'Os campos de número devem coincidir.'
        );


        // $this->validate($request, $regras, $mensagens);
        $validator = Validator::make($request->all(), $regras, $mensagens);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $data_hora = Carbon\Carbon::now();

        Recadastramento::Create([
            'data_hora' => $data_hora,
            'nome' => $request['nome'],
            'sexo' => $request['sexo'],
            'email' => $request['email'],
            'ddd' => $request['ddd'],
            'celular' => $request['numero'],
            'tribunal' => $request['tribunal'],
            'aposentado' => $request['aposentado'],
            'cidade' => $request['cidade'],
            'ok' => 0
        ]);

        $data_email = date('d-m-Y', strtotime($data_hora));

        $email = $request['email'];

        Mail::send('email.recadastro_usuario', 
            [
            'nome' => $request['nome'], 
            'sexo' => $request['sexo'], 
            'email' => $email, 
            'celular' => $request['ddd']." ".$request['numero'], 
            'aposentado' => $request['aposentado'], 
            'tribunal' => $request['tribunal'], 
            'cidade' => $request['cidade'],
            'data' => $data_email
            ], 
            function ($message) use ($email) {
                $message->from('formularios@sinjufego.org.br', 'Sinjufego');
                $message->to($email)->subject('Recadastramento Sinjufego');
                $message->to('administrativo@sinjufego.org.br')->subject('Recadastramento Sinjufego');
                $message->to('social@sinjufego.org.br')->subject('Recadastramento Sinjufego');
            }
        );

        return view('sucessoRecadastramento')->with('nome', $request['nome']);
    }

    public function form(){

        $tribunais = Tribunal::orderBy('tribunal','asc')->get();

        return view('index')->with('tribunais',$tribunais);
    }

    public function salvarForm(Request $request)
    {
        $regras = array(
            'nome' => 'required',
            'sexo' => 'required',
            'email' => 'required|confirmed',
            'email_confirmation' => 'required|same:email',
            'ddd' => 'required|confirmed',
            'numero' => 'required|confirmed',
            'ddd_confirmation' => 'required|same:ddd',
            'numero_confirmation' => 'required|same:numero',
            'aposentado' => 'required',
            'tribunal' => 'required',
            'cidade' => 'required'
        );

        $mensagens = array(
            'required' => 'O campo :attribute é obrigatório.',
            'numero.required' => 'O campo número é obrigatório.',
            'numero_confirmation.required' => 'O campo corfirmação de número é obrigatório.',
            'email_confirmation.required' => 'O campo corfirmação de email é obrigatório.',
            'ddd_confirmation.required' => 'O campo corfirmação de DDD é obrigatório.',
            'email.same' => 'Os campos de email devem coincidir.',
            'email_confirmation.same' => 'Os campos de email devem coincidir.',
            'ddd_confirmation.same' => 'Os campos de DDD devem coincidir.',
            'numero_confirmation.same' => 'Os campos de número devem coincidir.'
        );


        // $this->validate($request, $regras, $mensagens);
        $validator = Validator::make($request->all(), $regras, $mensagens);
        if ($validator->fails()) {
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $data_hora = Carbon\Carbon::now();


        Form::Create([
            'data_hora' => $data_hora,
            'nome' => $request['nome'],
            'sexo' => $request['sexo'],
            'email' => $request['email'],
            'ddd' => $request['ddd'],
            'celular' => $request['numero'],
            'tribunal' => $request['tribunal'],
            'aposentado' => $request['aposentado'],
            'cidade' => $request['cidade'],
            'ok' => 0
        ]);

        $data_email = date('d-m-Y', strtotime($data_hora));

        $email = $request['email'];

        Mail::send('email.cadastro_usuario', 
            [
            'nome' => $request['nome'], 
            'sexo' => $request['sexo'], 
            'email' => $email, 
            'celular' => $request['ddd']." ".$request['numero'], 
            'aposentado' => $request['aposentado'], 
            'tribunal' => $request['tribunal'], 
            'cidade' => $request['cidade'],
            'data' => $data_email
            ], 
            function ($message) use ($email) {
                $message->from('formularios@sinjufego.org.br', 'Sinjufego');
                $message->to($email)->subject('Cadastro Sinjufego');
                $message->to('administrativo@sinjufego.org.br')->subject('Cadastro Sinjufego');
                $message->to('social@sinjufego.org.br')->subject('Cadastro Sinjufego');
            }
        );

        return view('sucesso')->with('nome', $request['nome']);
    }

}