<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = "cadastro";

    public $timestamps = false;

    protected $fillable = array(
        'data_hora',
        'nome',
        'sexo',
        'email',
        'ddd',
        'celular',
        'tribunal',
        'aposentado',
        'cidade',
        'ok'
    );
}